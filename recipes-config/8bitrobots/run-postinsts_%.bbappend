FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://resize-rootfs.sh"

do_install_append() {
  install -d ${D}${sysconfdir}/rpm-postinsts
  install -m 0755 ${WORKDIR}/resize-rootfs.sh ${D}${sysconfdir}/rpm-postinsts
}
