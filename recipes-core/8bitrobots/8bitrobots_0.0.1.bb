DESCRIPTION = "8-Bit Robots"
HOMEPAGE = "http://8bitrobots.org"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fbbfbb98dab7dd2a2e481253113a05d7"

PROVIDES = "8bitrobots"
RPROVIDES_${PN} = "8bitrobots"

SRCREV = "master"
PR = "r1"
SRC_URI = "git://gitlab.com/8BitRobots/8bitrobots.git;protocol=https file://robot"

DEPENDS = " nodejs pigpio firstuse cmake-native boost"
RDEPENDS_${PN} = " nodejs pigpio firstuse boost hostapd dnsmasq softap wpa-supplicant avahi-daemon "
RDEPENDS_${PN} += "${@'xserver-xorg xf86-video-fbdev chromium-x11' if d.getVar('INCLUDE_CHROMIUM_8BITROBOTS') else ''}"

inherit npm-base update-rc.d systemd
INITSCRIPT_NAME = "robot"
INITSCRIPT_PARAMS="defaults 40"

S = "${WORKDIR}/git"

do_configure() {
}

do_compile() {
  export BOOST_LIBRARYDIR="${STAGING_DIR_TARGET}${libdir}"
  oe_runnpm install
}

do_install() {
  cp -R . ${D}/8bitrobots
  mkdir ${D}/8bitrobots/saved
  rm -rf ${D}/8bitrobots/node_modules/usage/compiled/*/ia32
  rm -rf ${D}/8bitrobots/node_modules/usage/compiled/*/x64
  rm -f ${D}/8bitrobots/utils/scurve
  rm -rf ${D}/8bitrobots/node_modules/node-gyp/test
  rm -rf ${D}/8bitrobots/node_modules/node-gyp/gyp/samples
  install -d ${D}${sysconfdir}/init.d
  install -m 0755 ${WORKDIR}/robot ${D}${sysconfdir}/init.d/robot
}

FILES_${PN} = "/8bitrobots ${sysconfdir}/init.d/robot"
